# Blender3D - Godot Casteroids
**Realitat virtual amb Blender, Godot i Arduino.**

[**_Tota la informació sobre BlenderCat 15a edició AQUÍ_**](CONTRIBUTING.md)

<img src="sprites/blender-cat-logo_300.png" alt="BlenderCat" width="300"> 
<img src="sprites/Godot_logo.png" alt="Godot Engine" width="300"> 
<img src="sprites/blender_logo_socket.png" alt="Blender3D" width="300"> 

## Descripció
Aquesta ponència consistirà en elaborar un petit space shooter per Realitat Virutal en un marge de 2h aproximadament. S’explicarà també com construir un
casc de realitat virtual des de zero basat en la plataforma Arduino que pugui
funcionar amb aquest joc.

El ponent proporcionarà dos cascs de realitat virtual per fer les proves, les llibreries necessàries per als sensors i una escena bàsica de Godot sobre la que
començar a treballar.

## Què necessiten els assistents?

*  Portàtil amb port USB. Si es vol provar el casc, l’ordinador ha de tenir port USB 3.0 i conector HDMI.
*  Godot 3.0
*  Arduino IDE
*  Un editor de text extern a Godot, com pot ser Notepad, Geany o Kate.

## Timing
Aquests seràn els punts que s’intentaràn cobrir durant la ponència, i el seu
temps aproximat.

Durant la primera hora intentarem construir l’escena de Godot i deixar-la preparada per al nostra casc de RV.

1.  Presentació (10 minuts): S’explicaràn els objectius de la classe, què és la plataforma Arduino, com funciona un casc de Realitat Virutal, etc.
2.  Construir l’escena de Godot Engine (20 minuts): colocar els objectes i assets a l’escena.
3.  Scripting (30 minuts): programar la nau del jugador per tal de poder-la controlar amb el Joystick. Programar la dels enemics perquè persegueixin al jugador.

Durant la segona part de la classe, acabarem de programar els scripts i començarem a treballar amb el casc de realitat virtual:
1.  Acabar de programar els Scripts, o pausa si s’han acabat (5 minuts)
2.  Explicar els components del casc i programa amb Arduino (20 minuts)
3.  Instalar Python-Godot i programar l’script de Realitat Virtual (20 minuts)
4.  Temps extra per preguntes, dubtes... (15 minuts)

## Equip de producció
*  [**Isaac de Palau** (Cap de projecte, VR)](CONTRIBUTING.md#isaac-de-palau)
*  [**Abraham Castilla** (Modelatge i texturitzat 3D - Cabina, pilot)](CONTRIBUTING.md#abraham-castilla)
*  [**Joan Betbese** (Modelatge i texturitzat 3D - Robots)](CONTRIBUTING.md#joan-betbesé)
*  [**Edelweiss Garcia** (Modelatge i texturitzat 3D - Missil)](CONTRIBUTING.md#edelweiss-garcia)
*  [**Richard** (Modelatge i texturitzat 3D - Space junk)](CONTRIBUTING.md#richard)
*  [**Jaume Castells** (Exportador Blender3D-Godot)](CONTRIBUTING.md#jaume-castells)
