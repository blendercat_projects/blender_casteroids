# Blendercat 15a edició
## Informació general
### Data
Dissabte 16 de novembre de 2019
### Lloc
ETECAM (Girona) https://etecam.cat/  https://www.casadelamusica.cat/ca/home/girones/_h:1/
### Adreça
Carrer Sant Antoni, 1, 1a Planta Factoria Cultural Coma Cros, 17190 Salt, Girona
### Telèfon
972 29 63 53
### Horari
9:00 - 14:20 / 16:00 - 20:00

## Organització
Associació **Blendercat** (Calella)
### President
**Abraham Castilla Herrezuelo** abrahamcastilla@gmail.com
### Secretari
**Edelweiss Gacía i Fuentes** dimonigirona@gmail.com
### Tresoré
**Marçal Mora Piquet** marcalmora72@gmail.com
### Descripció de l’associació
**Blendercat** es una associació no lucrativa que fomenta i ensenya el programari lliure 3D Blender i eines relacionades, arreu del territori de Catalunya per tot tipus de sector, amb la finalitat de que aquestes tecnologies siguin a l'abast de qualsevol tipus de persona. 


## Descripció de l’edició
La 15ª edició de BlenderCat es centra en ensenyar els fonaments bàsics per a l'exportació de models per a videojocs en Blender i el seu ús a diferents motors de joc. “Degut a l'eliminació del motor de joc natiu en Blender, veiem la necessitat de mostrar el workflow per a fer servir altres eines com GODOT i Verge3D, a més expandint el seu ús a diferents plataformes i medis actuals com el mòbil, web i Realitat Augmentada.”

## Programació
Taula horària

| **Ponència** | **Ponent** | **Descripció** | **Horari** |
| :---:    | :---: | :--- | :---: |
| Iniciació a Blender | [Edelweiss Garcia](#edelweiss-garcia) | Presentació blenderCat i programari. <br> <br> Formació bàsica per a iniciar-se amb Blender per a que els assistents que no hagin fet servir en Blender es puguin defensar en les ponències següents. | 9:00-10:00 |
| Grease Pencil + Verge3D | [Joan Betbesé](#joan-betbesé) | Introducció al workflow amb grease pencil 2D de Blender. <br> <br> Exportació de grease pencil per a aplicació de  virtualitzacions en web Verge3D. | 10:00-12:00 |
| Godot + Workflow per a videojocs | [Jaume Castells](#jaume-castells) | Project management amb CVS (Git, GitLab, GitKraken)  <br> <br> Exportació de models Blender3D 2.7x i 2.8x a Godot Game Engine. | 12:20-14:20 |
| Assets Low Poly | [Abraham Castilla](#abraham-castilla) | Explicació del modelat dels assets Low Poly per a exportar a Godot. | 16:00-16:30 |
| Godot + VR | [Isaac de Palau](#isaac-de-palau) | Configuració de minijoc a Godot.  <br> <br> Visualització i configuració amb Arduino per a Realitat Virtual amb Godot. | 16:30-18:30 |
| Novetats de Blender | [Abraham Castilla](#abraham-castilla) | Noticies de les noves funcionalitats i novetats de Blender 2.81-2.82. | 16:30-18:30 |


## Necessitats de l'aula de ponències
* Aula per a entre 10 i 30 persones aproximadament, sapiguer el límit d'assistents per tancar inscripcions.
* Projector per als ponents.
* Altaveus per a reproducció de videos.
* Endolls per als equips dels assistents.
* Alguns ordinadors per qui no pugui portar el seu equip.
* Connexió a internet com a mínim per al ponent.

## Software a utilitzar
* Blender
* Godot
* Arduino IDE
* Editor de text
* Navegador web

## Preu per assistent
10€ per a cobrir les necessitats bàsiques del ponents i organitzadors; inclou la quota d'inscripció dels assistents a Blendercat fins a cap d´any.


Per ordre de valoració de lo recaptat per als ponents:
1. Com a mínim el menjar 
2. Si es pot, el trasllat
3. Si n´hi ha, pagar les ponències proporcionalment
4. A més s'ha de valorar i acordar el cost de les instal·lacions, o en tot cas es pot acordar un percentatge de lo recaptat. 

> **Blendercat** es una associació no lucrativa i tots els beneficis es fan servir per a la realització dels events i promoure blender arreu de Catalunya.  

# Altres

## Publicitat i difussió
* **BlenderCat** s´anunciará per xarxes socials habituals; facebook, twitter, instagram, etc,... en les que ja té usuaris interessats, a més de les d'us intern; whatsapp, telegram, gmail,..., sense arribar a fer spam ja que també hi són per a usuaris interessats.
* La web no es pot fer servir actualment per canvi de hosting, tot i que es proveirà d'una pàgina provisional per adreçar i atendre als interessats a més de donar l´informació oficialment.
* També es faran fer servir canals oficials de la fundació i comunitat blender, com el blender Network i el Blender community, i segurament es sumaran altres webs dedicades e interessades amb blender.
* Potser es fará una retransmissió en directe o es grabará en vídeo tota o part de la edició.
* Per a la publicitat es proveirà d´un flyer digital i si cau s´imprimiran cartells per a enganxar a punts que siguin d'interès. Per a la realització d'aquest flyer es demanarà i es farà servir logotips de les entitats col·laboradores, softwares, empreses o que siguin indicadors del que es farà a l´edició corresponent per a fer servir en tots els canals d'informació i difusió.
* Revista 15a edició: com es costum es proposa realitzar després de la 15ª edició, una revista digital amb informació referent amb el que s'ha fet durant les sessions i així tenir material escrit de les ponències, a la revista ahuran anuncis de tots els promotors i col·laboradors relacionats.
* Enllaç a les revistes anteriors: https://issuu.com/bamspa

## Informació ponents

### Edelweiss Garcia

### Joan Betbesé
*  revoltsrock@gmail.com
*  https://sitconsstudio.com/ Empresa dedicada a virtualitzacions de Verge3D amb Blender.

### Jaume Castells
*BFCT especialitzat en programació de videojocs (Unity3D, Godot, Unreal,..)*
*  zammecas@gmail.com
*  https://www.vilogamesstudio.com/

### Abraham Castilla
*Formador i generalista 3D especialitzat en Blender*
*  https://www.blendernetwork.org/abraham-castilla
*  http://abrahamartdesign.com

### Isaac de Palau
*  idepalau@gmail.com
*  https://robologs.net/

