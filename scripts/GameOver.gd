extends Control


const WIN_MESSAGE : String = "You Win!"
const LOSE_MESSAGE : String = "You Lose!"

onready var ok_button : Button = find_node("OkButton")

var message_node : Label

var game_manager


func _ready():
	game_manager = get_parent()
	message_node = find_node("ResultLabel")
	_autoset_scene()


func _autoset_scene():
	if game_manager is GameManager:
		if game_manager.last_result_win:
			set_win()
		else:
			set_lose()
	ok_button.grab_focus()


func set_message(message : String):
	message_node.text = message


func set_lose():
	set_message(LOSE_MESSAGE)


func set_win():
	set_message(WIN_MESSAGE)


func _on_OkButton_button_up():
	if game_manager is GameManager:
		game_manager.exit_to_main_menu()
