extends KinematicBody

#Vida del Drone
var HP

#Velocidad del drone
var speed

#Objetivo del drone
var target

#Variable auxiliar para el movimiento
var velocity

#Daño que causa el drone
var dany_baliza
var dany_jugador

# Collision shapes
var collision_shape : CollisionShape

var drone_enabled : bool = false

var drone_collision : KinematicCollision = null
var last_object_touched = null
var obstacles_area : Area = null

# Stage manager link
var stage_manager : Node

var on_camera : bool = false

var target_assigned : TextureRect = null
var marker_assigned : TextureRect = null

var drone_paused : bool = false


func _ready():
	get_collision_shape()
	#obstacles_area = get_node("/root/Ship/Obstacles_Area")
	stage_manager = get_node("/root/GameManager/StageManager")


func aim():
	#Calcular la rotación deseada hacia el objetivo
	look_at(target.global_transform.origin, Vector3.UP)


func disable_drone():
	drone_paused = true
	on_camera = false
	if target_assigned:
		target_assigned.set_target_enabled(false)
	if marker_assigned:
		marker_assigned.set_marker_enabled(false)
	target_assigned = null
	marker_assigned = null
	visible = false
	collision_shape.disabled = true
	drone_enabled = false
	#print("Drone", name, "disabled!")


func enable_drone(hp, objectiu, spd, dj, db):
	inicialitzar_parametres(hp, objectiu, spd, dj, db)
	last_object_touched = null
	drone_enabled = true
	visible = true
	collision_shape.disabled = false
	if !on_camera:
		if target_assigned:
			target_assigned.set_target_enabled(false)
			target_assigned = null
		marker_assigned = stage_manager.ship.hud.drone_out_camera(self, get_viewport().get_camera())
	else:
		if marker_assigned:
			marker_assigned.set_marker_enabled(false)
			marker_assigned = null
		target_assigned = stage_manager.ship.hud.drone_in_camera(self, get_viewport().get_camera())
	drone_paused = false


func get_collision_shape():
	collision_shape = get_node("CollisionShape")


func inicialitzar_parametres(hp, objectiu, spd, dj, db):
	#Inicializar parámetros del dron
	self.HP = hp
	self.target = objectiu
	self.speed = spd
	self.dany_baliza = db
	self.dany_jugador = dj


func kill_drone():
	if drone_enabled:
		disable_drone()
		stage_manager.enemy_killed()


func pause_drone():
	drone_paused = true


func process_collision():
	#print("Current touched", drone_collision.collider.name)
	#if last_object_touched != null:
		#print("Last touched", last_object_touched.name)
	if drone_collision.collider != last_object_touched:
		var collider_name : String = drone_collision.collider.name
		stage_manager.show_fire_explosion(global_transform.origin)
		if "Obstacle" in collider_name:
			stage_manager.show_asteroid_explosion(global_transform.origin)
			disable_drone()
		elif "Ship" in collider_name:
			stage_manager.show_fire_explosion(global_transform.origin)
			kill_drone()
		elif "Beacon" in collider_name:
			stage_manager.ship.rebre_dany_baliza(dany_baliza)
			disable_drone()
		else:
			#rebre_dany(10.0)
			kill_drone()
		last_object_touched = drone_collision.collider


func unpause_drone():
	drone_paused = false


func _physics_process(dt):
	if drone_paused:
		return
	
	if drone_enabled:
		#Encarar el dron hacia su objetivo
		aim()
		var facing = global_transform.basis.z * -speed * dt
		drone_collision = move_and_collide(facing)
		if drone_collision:
			process_collision()
		
		if on_camera:
			pass


func rebre_dany(valor):
	#Calcular la pérdida de vida
	self.HP -= valor
	
	#Destruir el dron si la vida es inferior a 0
	if self.HP <= 0:
		disable_drone()


func _on_VisibilityNotifier_camera_entered(camera):
	on_camera = true
	if marker_assigned:
		marker_assigned.set_marker_enabled(false)
		marker_assigned = null
	target_assigned = stage_manager.ship.hud.drone_in_camera(self, camera)


func _on_VisibilityNotifier_camera_exited(camera):
	on_camera = false
	if target_assigned:
		target_assigned.set_target_enabled(false)
		target_assigned = null
	marker_assigned = stage_manager.ship.hud.drone_out_camera(self, camera)
