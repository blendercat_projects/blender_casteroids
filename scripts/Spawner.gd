extends Spatial

export var gunners : int = 2
export var watchers : int = 2

const DRONES_PARENT_NAME : String = "Drones"

var spawn_timer : Timer = null
var gunner_drones_pool = []
var watcher_drones_pool = []
var i_next_available_gunner : int = -1
var i_next_available_watcher : int = -1

# Links to avoid searching delays in game
#var beacon_link
var drones_parent : Spatial
var game_manager : Node

var spawners


func _ready():
	game_manager = get_parent()
	drones_parent = Spatial.new()
	drones_parent.name = DRONES_PARENT_NAME
	game_manager.add_child(drones_parent)
	spawners = get_children()
	
	# Generate drones instances before in game to avoid loading delays
	fill_gunner_drones_pool()
	fill_watcher_drones_pool()
	
	create_timer()
	add_child(spawn_timer)


func create_timer():
	spawn_timer = Timer.new()
# warning-ignore:return_value_discarded
	spawn_timer.connect("timeout",self,"_crear_drone") 


func enable_timer():
	set_random_time()
	spawn_timer.start()


func create_gunner(index : int):
	var drone = load("res://scenes/Drone_gunner.tscn").instance()
	drone.name = "Gunner" + str(index)
	gunner_drones_pool.append(drone)
	drones_parent.add_child(drone)
	drone.disable_drone()


func create_watcher(index : int):
	var drone = load("res://scenes/Drone_watcher.tscn").instance()
	drone.name = "Watcher" + str(index)
	watcher_drones_pool.append(drone)
	drones_parent.add_child(drone)
	drone.disable_drone()


func disable_drone(drone : KinematicBody):
	drone.disable_drone()


func enable_gunner():
	# First, get a random spawner
# warning-ignore:unused_variable
	var r_spawner : Spatial = get_random_spawner()
	# Then, enable an available drone
	update_next_available_gunner()
	if i_next_available_gunner > -1:
		gunner_drones_pool[i_next_available_gunner].transform.origin = r_spawner.transform.origin
		gunner_drones_pool[i_next_available_gunner].enable_drone(500, game_manager.ship, 10, 20, 100)


func enable_watcher():
	# First, get a random spawner
# warning-ignore:unused_variable
	var r_spawner : Spatial = get_random_spawner()
	# Then, enable an available drone
	update_next_available_watcher()
	if i_next_available_watcher > -1:
		watcher_drones_pool[i_next_available_watcher].transform.origin = r_spawner.transform.origin
		watcher_drones_pool[i_next_available_watcher].enable_drone(100, game_manager.beacon, 5, 5, 1000)


func fill_gunner_drones_pool():
# warning-ignore:unused_variable
	for i_drone in range(gunners):
		create_gunner(i_drone)


func fill_watcher_drones_pool():
# warning-ignore:unused_variable
	for i_drone in range(watchers):
		create_watcher(i_drone)


func get_random_spawner():
	return spawners[randi()%(len(spawners))]


func pause_drones():
	for dw in watcher_drones_pool:
		dw.pause_drone()
	for dg in gunner_drones_pool:
		dg.pause_drone()


func pause_spawner():
	stop_timer()


func resume_timer():
	spawn_timer.paused = false


func set_random_time():
	#Generar un numero aleatorio entre 15 y 35
	#Cambiar el tiempo de spawning
	#spawn_timer.wait_time = 15+randi()%20 +1
	spawn_timer.wait_time = 2+randi()%5 +1


func start():
	# Turn drone spawner timer on
	enable_timer()


func stop_timer():
	spawn_timer.paused = true


func unpause_drones():
	for dw in watcher_drones_pool:
		dw.unpause_drone()
	for dg in gunner_drones_pool:
		dg.unpause_drone()


func unpause_spawner():
	resume_timer()


func update_next_available_gunner():
	i_next_available_gunner = 0
	while ((i_next_available_gunner < gunners) and (gunner_drones_pool[i_next_available_gunner].drone_enabled)):
		i_next_available_gunner += 1
	if i_next_available_gunner == gunners:
		i_next_available_gunner = -1


func update_next_available_watcher():
	i_next_available_watcher = 0
	while ((i_next_available_watcher < watchers) and (watcher_drones_pool[i_next_available_watcher].drone_enabled)):
		i_next_available_watcher += 1
	if i_next_available_watcher == watchers:
		i_next_available_watcher = -1


func _crear_drone():
	#Paso 0 - Generar semilla aleatoria
	randomize()
	
	#Paso 1 - elegir un tipo de drone:
	var eleccion = randi()%2 #numero aleatorio entre 0 y 1
	#print(eleccion)
	
	#Paso 2 - instanciar el drone y definir sus atributos según la variante
	if eleccion == 0:
		#print("Gunner choosed!")
		enable_gunner()
	else:
		#print("Watcher choosed!")
		enable_watcher()
	
	enable_timer()
