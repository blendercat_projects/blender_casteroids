extends Area

const OBSTACLES_PARENT_PATH : String = "res://scenes/Obstacles.tscn"

export var n_random_obstacles : int = 50

export var obstacles_paths : Array

var obstacles_parent_node : Spatial
#var obstacles_packedscenes : Array
var obstacle_instances = []

var area_radius : float


func _ready():
	obstacles_parent_node = get_obstacles_parent_node()
	area_radius = $ObstaclesAreaShape.shape.radius


func get_mirrored_position(obstacle_current_position : Vector3):
	var mirrored_position : Vector3 = Vector3.ZERO
	
	var _distance = translation.distance_to(obstacle_current_position)
	
	return mirrored_position


func get_new_random_position():
	var random_position : Vector3 = Vector3.ZERO
	random_position.x = rand_range(-area_radius, area_radius)
	random_position.y = rand_range(-area_radius, area_radius)
	random_position.z = rand_range(-area_radius, area_radius)
	return random_position


func get_new_random_scale():
	var random_scale_factor : float = rand_range(1.0, 3.0)
	var random_scale : Vector3 = Vector3(random_scale_factor, random_scale_factor, random_scale_factor)
	return random_scale


func get_obstacles_parent_node():
	var mn = load(OBSTACLES_PARENT_PATH).instance()
	var main_node = get_node("../..") 
	main_node.add_child(mn)
	return mn


func get_random_obstacle_scene_index():
	var n_obs : int = 0
	n_obs = randi()%(len(obstacles_paths))
	
	return n_obs


func init_random_obstacles():
	randomize()
# warning-ignore:unused_variable
	for n_obs in range(0, n_random_obstacles):
		var obstacle : StaticBody = load(obstacles_paths[get_random_obstacle_scene_index()]).instance()
		obstacle.name = "Obstacle" + str(n_obs)
		obstacles_parent_node.add_child(obstacle)
		# Set space random position
		obstacle.translation = get_new_random_position()
		# Set random scale
		obstacle.scale = get_new_random_scale()
		# Set random rotation
		rotate_random_rotation(obstacle)
		obstacle_instances.append(obstacle)


func obstacle_touched_by_ship(obstacle):
	obstacle.visible = false
	realocate_obstacle(obstacle)
	obstacle.visible = true


func realocate_mirrored_obstacle(obs : StaticBody):
	# TODO: s'ha de calcular el reflexe!!!
	#var distance = translation.distance_to(obs.translation)
	#print(obs.name, " at ", obs.global_transform.origin, " a ", global_transform.origin)
	obs.translation = get_mirrored_position(obs.translation)
	obs.scale = get_new_random_scale()
	rotate_random_rotation(obs)


func realocate_obstacle(obs : StaticBody):
	obs.translation = get_new_random_position()
	obs.scale = get_new_random_scale()
	rotate_random_rotation(obs)


func rotate_random_rotation(obstacle : StaticBody):
	obstacle.rotate_x(rand_range(0.0, 359.9))
	obstacle.rotate_y(rand_range(0.0, 359.9))
	obstacle.rotate_z(rand_range(0.0, 359.9))


func _on_Obstacles_Area_body_exited(body):
	if "Obstacle" in body.name:
		#print(body.name, " out")
		realocate_mirrored_obstacle(body)
		#realocate_obstacle(body)
