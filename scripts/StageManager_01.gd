extends Node

# PATHS
const ASTEROIDS_EXPLOSIONS_PATH : String = "res://scenes/Asteroid_explosion.tscn"
const FIRE_EXPLOSIONS_PATH : String = "res://scenes/Explosion_effect.tscn"
const SHIP_PATH : String = "res://scenes/Ship.tscn"
const SHIP_VR_PATH : String = "res://scenes/Ship_VR.tscn"
const SPAWNER_CONTAINER : String = "res://scenes/Spawner_container.tscn"
const BEACON_PATH : String = "res://scenes/Beacon.tscn"
const PAUSE_MENU_PATH : String = "res://scenes/PauseMenu.tscn"

# NAMES
const SHIP_NAME : String = "Ship"
const EFFECTS_PARENT_NAME : String = "Effects"
const ASTEROIDS_EXPLOSIONS_PARENT_NAME : String = "Asteroids_explosions"
const FIRE_EXPLOSIONS_PARENT_NAME : String = "Fire_explosions"
const PAUSE_MENU_NAME : String = "Pause_menu"

export var enemies_to_kill : int = 5

var ship_spawner : Position3D
var ship : KinematicBody
var spawner_container : Spatial
var beacon : Spatial
var hub : Control

var asteroids_explosions = []
var fire_explosions = []
var available_asteroid_explosion_index : int = 0
var available_fire_explosion_index : int = 0

var fade : Tween

var pause_menu : WindowDialog = null

var game_manager : Node

var enemies_killed : int = 0


func _ready():
	game_manager = get_parent()
	game_manager.create_fade(self)
	prepare_stage()
	start_stage()


func add_ship():
	var ship_resource = load(SHIP_PATH)
	ship = ship_resource.instance()
	ship.name = SHIP_NAME


func add_VR_ship():
	var ship_resource = load(SHIP_VR_PATH)
	ship = ship_resource.instance()
	ship.name = SHIP_NAME


func add_spawner_container():
	var sp = load(SPAWNER_CONTAINER)
	spawner_container = sp.instance()
	add_child(spawner_container)


func check_stage():
	if enemies_killed >= enemies_to_kill:
		end_stage(true)


func create_asteroid_explosion(i_name : String):
	var explosion : Particles = load(ASTEROIDS_EXPLOSIONS_PATH).instance()
	explosion.name = "Asteroid_explosion" + i_name
	return explosion


func create_fire_explosion(i_name : String):
	var explosion : Particles = load(FIRE_EXPLOSIONS_PATH).instance()
	explosion.name = "Fire_explosion" + i_name
	return explosion


func create_pause_menu():
	pause_menu = load(PAUSE_MENU_PATH).instance()
	pause_menu.name = PAUSE_MENU_NAME
	pause_menu.visible = false
	add_child(pause_menu)


func disable_ship_control():
	ship.enable_control(false)


func enable_ship_control():
	ship.enable_control(true)


func end_stage(won : bool):
	print("End stage")
	game_manager.last_result_win = won
	game_manager.game_state = game_manager.GAME_STATE.ENDSTAGE
	game_manager.fade_in()



func enemy_killed():
	enemies_killed += 1
	check_stage()


func faded_out():
	start_stage()


func get_ship_spawner():
	ship_spawner = get_node("Ship_spawner")


func load_asteroids_explosions(number : int, obj_parent : Spatial):
	var asteroids_explosion_parent : Spatial = Spatial.new()
	asteroids_explosion_parent.name = ASTEROIDS_EXPLOSIONS_PARENT_NAME
	obj_parent.add_child(asteroids_explosion_parent)
	for _i_exp in range(number):
		var ast_exp = create_asteroid_explosion(str(number))
		asteroids_explosion_parent.add_child(ast_exp)
		asteroids_explosions.append(ast_exp)


func load_beacon():
	beacon = load(BEACON_PATH).instance()
	var beacon_spawner : Spatial = get_node("Beacon_spawner")
	add_child(beacon)
	beacon.global_transform.origin = beacon_spawner.global_transform.origin


func load_effects():
	var effects_parent : Spatial = Spatial.new()
	effects_parent.name = EFFECTS_PARENT_NAME
	load_asteroids_explosions(4, effects_parent)
	load_fire_explosions(4, effects_parent)
	add_child(effects_parent)


func load_fire_explosions(number : int, obj_parent : Spatial):
	var fire_explosion_parent : Spatial = Spatial.new()
	fire_explosion_parent.name = FIRE_EXPLOSIONS_PARENT_NAME
	obj_parent.add_child(fire_explosion_parent)
	for _i_exp in range(number):
		var fire_exp = create_fire_explosion(str(number))
		fire_explosion_parent.add_child(fire_exp)
		fire_explosions.append(fire_exp)


func load_nonvr_stage():
	#load_nonvr_settings()
	add_ship()


func load_pause_menu():
	if !pause_menu:
		create_pause_menu()
	pause_menu.popup_centered()


func load_player():
	if ship_spawner:
		if game_manager.vr_enabled:
			# Add Ship_VR scene
			load_vr_stage()
		else:
			# Add Ship scene
			load_nonvr_stage()
		add_child(ship)
		set_ship_position()
	else:
		print("Ship spawner not found!")


func load_vr_stage():
	#load_vr_settings()
	add_VR_ship()


func pause_drones():
	spawner_container.pause_drones()


func pause_game():
	pause_player()
	pause_drones()
	pause_spawner()
	load_pause_menu()


func pause_player():
	ship.pause_ship()


func pause_spawner():
	spawner_container.pause_spawner()


func prepare_stage():
	create_pause_menu()
	load_beacon()
	load_effects()
	get_ship_spawner()
	load_player()


func set_next_asteroid_explosion_effect():
	available_asteroid_explosion_index += 1
	if available_asteroid_explosion_index == len(asteroids_explosions):
		available_asteroid_explosion_index = 0


func set_next_fire_explosion_effect():
	available_fire_explosion_index += 1
	if available_fire_explosion_index == len(fire_explosions):
		available_fire_explosion_index = 0


func set_new_scene():
	game_manager.load_game_over()
	queue_free()


func set_ship_position():
	ship.transform.origin = ship_spawner.transform.origin


func show_asteroid_explosion(pos : Vector3):
	asteroids_explosions[available_asteroid_explosion_index].fire(pos)
	set_next_asteroid_explosion_effect()


func show_fire_explosion(pos : Vector3):
	fire_explosions[available_fire_explosion_index].fire(pos)
	set_next_fire_explosion_effect()


func start():
	spawner_container.start()


func start_stage():
	add_spawner_container()
	ship.autoset_obstacles_area()
	ship.init_random_obstacles()
	unpause_player()
	game_manager.game_state = game_manager.GAME_STATE.GAME
	game_manager.fade_out()


func unload_pause_menu():
	if !pause_menu:
		create_pause_menu()
	pause_menu.hide()
	game_manager.sound_manager.play_ingame_music()


func unpause_drones():
	spawner_container.unpause_drones()


func unpause_game():
	unload_pause_menu()
	unpause_player()
	unpause_drones()
	unpause_spawner()
	game_manager.game_state = game_manager.GAME_STATE.GAME


func unpause_player():
	ship.unpause_ship()


func unpause_spawner():
	spawner_container.unpause_spawner()


