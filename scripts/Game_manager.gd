class_name GameManager extends Node

# FOR TESTING
enum DIRECT_LOADING { NONE, CONTRIBUTORS, MADEWITH, TITLE, MAINMENU, GAME}
export (DIRECT_LOADING) var direct_game_loading = DIRECT_LOADING.NONE

export var DEFAULT_VR_SCREEN : Vector2
export var DEFAULT_NONVR_SCREEN : Vector2
export var vr_enabled : bool

enum GAME_STATE {NONE, MAIN_MENU, GAME, PAUSE_MENU, ANIMATION, TITLES, LOADING, ENDSTAGE}
var game_state = GAME_STATE.NONE

# PATHS
const SOUND_MANAGER_PATH : String = "res://scenes/SoundManager.tscn"
const GAME_TITLE_PATH : String = "res://scenes/GameTitle.tscn"
const CONTRIBUTORS_PATH : String = "res://scenes/Contributors.tscn"
const MADE_WITH_PATH : String = "res://scenes/MadeWith.tscn"
const MAIN_MENU_PATH : String = "res://scenes/MainMenu.tscn"
const STAGE_MANAGER_PATH : String = "res://scenes/stages/StageManager_0000.tscn" # To modify at load stage
const LOADING_PATH : String = "res://scenes/Loading.tscn"
const FADER_PANEL_PATH : String = "res://scenes/SceneFader.tscn"
const GAME_OVER_PATH : String = "res://scenes/GameOver.tscn"

var current_resolution : Vector2
var loading_scene : Control
var sound_manager : Node = null

# For loading scenes
var scene_to_load_path : String = ""
var current_scene
var fader_panel : Tween

# Loading saved game
var is_saved_game : bool = false

var last_result_win : bool = false


func _ready():
	create_sound_manager()
	config_app()
	# FOR TESTING
	match direct_game_loading:
		DIRECT_LOADING.NONE:
			start_app()
		DIRECT_LOADING.CONTRIBUTORS:
			load_contributors()
		DIRECT_LOADING.MADEWITH:
			load_made_with()
		DIRECT_LOADING.TITLE:
			load_game_title()
		DIRECT_LOADING.MAINMENU:
			load_main_menu()
		DIRECT_LOADING.GAME:
			load_stage(1)


func config_app():
	load_settings()


func create_fade(parent):
	fader_panel = load(FADER_PANEL_PATH).instance()
	parent.add_child(fader_panel)


func create_loading_scene():
	loading_scene = load(LOADING_PATH).instance()
	add_child(loading_scene)


func create_sound_manager():
	sound_manager = load(SOUND_MANAGER_PATH).instance()
	add_child(sound_manager)


func delete_fader():
	fader_panel.queue_free()


func exit_to_main_menu():
	current_scene.queue_free()
	load_main_menu()


func fade_in():
	fader_panel.fade_in()


func fade_out():
	fader_panel.fade_out()


func get_saved_game():
	var saved : bool = false
	# TODO: LOAD SAVED GAME
	return saved


func load_contributors():
	current_scene = load(CONTRIBUTORS_PATH).instance()
	add_child(current_scene)


func load_game_over():
	current_scene = load(GAME_OVER_PATH).instance()
	add_child(current_scene)


func load_game_title():
	current_scene = load(GAME_TITLE_PATH).instance()
	add_child(current_scene)


func load_made_with():
	current_scene = load(MADE_WITH_PATH).instance()
	add_child(current_scene)


func load_main_menu():
	game_state = GAME_STATE.MAIN_MENU
	scene_to_load_path = MAIN_MENU_PATH
	sound_manager.play_mainmenu_music()
	create_loading_scene()


func load_nonvr_settings():
	OS.set_window_title("Asteroids Non-VR")
	OS.set_window_size(DEFAULT_NONVR_SCREEN)
	current_resolution = DEFAULT_NONVR_SCREEN
	set_nonvr_window_position()


func load_settings():
	if vr_enabled:
		load_vr_settings()
	else:
		load_nonvr_settings()
	is_saved_game = get_saved_game()


func load_stage(number : int = 1):
	var stage_path : String = STAGE_MANAGER_PATH
	var number_string_format : String = str(number).pad_zeros(4)
	stage_path = stage_path.replace("0000", number_string_format)
	if ResourceLoader.exists(stage_path):
		create_loading_scene()
		loading_scene.load_scene(stage_path)
		sound_manager.play_ingame_music()
	else:
		print("Stage number not found.")
		print("Loading main menu.")


func load_titles():
	game_state = GAME_STATE.TITLES
	sound_manager.play_mainmenu_music()
	title_showed(self)


func load_vr_settings():
	OS.set_window_title("Asteroids VR")
	OS.set_window_size(DEFAULT_VR_SCREEN)
	current_resolution = DEFAULT_VR_SCREEN


func pass_current_title():
	current_scene.force_quit()


func pause_game():
	game_state = GAME_STATE.PAUSE_MENU
	sound_manager.pause_music_stream()
	sound_manager.play_pausemenu_music()
	current_scene.pause_game()


func process_cancel_input():
	match game_state:
		GAME_STATE.NONE:
			pass
		GAME_STATE.MAIN_MENU:
			pass
		GAME_STATE.GAME:
			pause_game()
		GAME_STATE.PAUSE_MENU:
			unpause_game()
		GAME_STATE.ANIMATION:
			pass
		GAME_STATE.TITLES:
			pass_current_title()
		GAME_STATE.LOADING:
			pass


func quit():
	# TODO: Ask for sure
	get_tree().quit()


func set_nonvr_window_position():
	var screen_size = OS.get_screen_size()
#	OS.set_window_size(screen_size*0.5)
	var window_size = OS.get_window_size()
	OS.set_window_position(screen_size*0.5 - window_size*0.5)


func start_app():
	load_titles()


func title_showed(title_node):
	var node_name = title_node.name
	match node_name:
		"Contributors":
			title_node.queue_free()
			load_made_with()
		"MadeWith":
			title_node.queue_free()
			load_game_title()
		"GameTitle":
			title_node.queue_free()
			load_main_menu()
		_:
			load_contributors()


func unpause_game():
	game_state = GAME_STATE.GAME
	sound_manager.play_ingame_music()
	current_scene.unpause_game()


func _input(event):
	# User input is catched all the time
	if event.is_action_pressed("ui_cancel"):
		process_cancel_input()
	
	# For testing purposes
	if event == InputEventScreenTouch:
		print("Mouse...")
		print(event.relative)
