extends Tween

export var fade_time : float = 2.0

var scene_parent
var fading : bool
var started : bool


func _ready():
	autoset_scene_parent()


func adjust_fader_panel():
	$FaderPanel.rect_min_size = get_viewport().get_visible_rect().size


func autoset_scene_parent():
	scene_parent = get_parent()


func fade_in():
	adjust_fader_panel()
	#var initial_color : Color = Color(1,1,1,0)
	#var final_color : Color = Color(1,1,1,1)
	fading = self.interpolate_property($FaderPanel, "modulate:a", 0, 1, fade_time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	started = self.start()


func fade_out():
	adjust_fader_panel()
	#var initial_color : Color = Color(1,1,1,1)
	#var final_color : Color = Color(1,1,1,0)
	fading = self.interpolate_property($FaderPanel, "modulate:a", 1, 0, fade_time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	started = self.start()


func faded_in():
	if scene_parent != null:
		scene_parent.set_new_scene()
		queue_free()


func faded_out():
	scene_parent.start()


func _on_SceneFader_tween_completed(object, _key):
	#print(object, " changed ", key)
	if object.modulate.a == 0:
		faded_out()
	elif object.modulate.a == 1:
		faded_in()
