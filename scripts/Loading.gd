extends Control


const SIMULATED_DELAY_SEC = 0.1


var loader
var wait_frames
var time_max = 100 # msec
var current_scene
var timer : Timer
var game_manager : Node
var fader : Tween
var loaded_resource

var loading_bar : ColorRect
var bar_length : float = 400.0


func _ready():
	game_manager = get_parent()
	game_manager.game_state = game_manager.GAME_STATE.LOADING
	timer = find_node("Timer")
	fader = find_node("SceneFader")
	loading_bar = find_node("Bar")
	loading_bar.rect_scale.x = 0
	start_loading()


func goto_scene(): # game requests to switch to this scene
	loader = ResourceLoader.load_interactive(game_manager.scene_to_load_path)
	if loader == null: # check for errors
		show_error()
		return
	set_process(true)
	
	#current_scene.queue_free() # get rid of the old scene
	
	# start your "loading..." animation
	#get_node("animation").play("loading")
	
	wait_frames = 1


func load_scene(path : String = ""):
	game_manager.scene_to_load_path = path


func pause_before_loading(pause_time : float = 1.0):
	timer.wait_time = pause_time
	timer.start(timer.wait_time)


func set_new_scene():
	game_manager.current_scene = loaded_resource.instance()
	game_manager.add_child(game_manager.current_scene)
	queue_free()


func show_error():
	print("error")


func start():
	goto_scene()


func start_loading():
	fader.fade_out()


func update_progress():
	var progress = float(loader.get_stage()) / loader.get_stage_count()
	print(progress)
	loading_bar.rect_scale.x = progress
	yield(get_tree(),"idle_frame")


func _process(_time):
	if loader == null:
		# no need to process anymore
		set_process(false)
		return
	
	if wait_frames > 0: # wait for frames to let the "loading" animation to show up
		wait_frames -= 1
		return
	
	var t = OS.get_ticks_msec()
	while OS.get_ticks_msec() < t + time_max: # use "time_max" to control how much time we block this thread
		# poll your loader
		var err = loader.poll()
	
		if err == ERR_FILE_EOF: # load finished
			loaded_resource = loader.get_resource()
			loader = null
			loading_bar.rect_scale.x = 1.0
			fader.fade_in()
			break
		elif err == OK:
			update_progress()
			OS.delay_msec(int(SIMULATED_DELAY_SEC * 1000.0))
		else: # error during loading
			show_error()
			loader = null
			break
