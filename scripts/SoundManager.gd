extends Node

# Audio settings
export var music_on : bool = true
export var fx_on : bool = true
export var music_volume : float = 1.0
export var fx_volume : float = 1.0
export var streams_number : int = 4

var ingame_music = load("res://sound/music/Game_The_End_Is_Near.ogg")
var main_menu_music = load("res://sound/music/MainMenu_Keys.ogg")
var pause_menu_music = load("res://sound/music/PauseMenu_Demon.ogg")

var music_playing = ""
var music_player : AudioStreamPlayer

var audio_streams = []
var ingame_music_stream_position = 0.0
var mainmenu_music_stream_position = 0.0
var pausemenu_music_stream_position = 0.0

onready var game_manager


func _ready():
	game_manager = get_parent()
	create_audio_streams()


func create_audio_streams():
	music_player = AudioStreamPlayer.new()
	music_player.name = "MusicStream"
	music_player.set_bus("Music")
	add_child(music_player)

	for n_stream in range(streams_number):
		var a_stream : AudioStreamPlayer = AudioStreamPlayer.new()
		a_stream.name = "AudioStream" + str(n_stream)
		a_stream.set_bus("SFXs")
		add_child(a_stream)
		audio_streams.append(a_stream)


func load_ingame_music():
	if music_playing == "ingame":
		return
	stop_stream(0)
	music_playing = "ingame"
	music_player.stream = ingame_music


func load_mainmenu_music():
	if music_playing == "mainmenu":
		return
	stop_stream(0)
	music_playing = "mainmenu"
	music_player.stream = main_menu_music


func load_pausemenu_music():
	if music_playing == "pausemenu":
		return
	stop_stream(0)
	music_playing = "pausemenu"
	music_player.stream = pause_menu_music


func loop_music_stream():
	audio_streams[0].loop = true


func pause_music_stream():
	match music_playing:
		"mainmenu":
			mainmenu_music_stream_position = music_player.get_playback_position()
		"ingame":
			ingame_music_stream_position = music_player.get_playback_position()
		"pausemenu":
			pausemenu_music_stream_position = music_player.get_playback_position()
	music_player.stop()


func play_audio_stream(number, position = 0.0):
	if number == 0:
		if music_on:
			audio_streams[number].set_volume_db(music_volume)
			audio_streams[number].play(position)
	else:
		if fx_on:
			audio_streams[number].set_volume_db(fx_volume)
			audio_streams[number].play(position)


func play_environment_stream():
	play_audio_stream(1)


func play_ingame_music():
	load_ingame_music()
	resume_music_stream()


func play_mainmenu_music():
	load_mainmenu_music()
	resume_music_stream()


func play_menuclick_stream():
	play_audio_stream(2)


func play_menucancel_stream():
	play_audio_stream(3)


func play_music_stream(_pos):
	music_player.play(_pos)


func play_pausemenu_music():
	load_pausemenu_music()
	resume_music_stream()


func play_playerstep_stream():
	play_audio_stream(4)


func play_playerjump_stream():
	play_audio_stream(5)


func play_playerdead_stream():
	play_audio_stream(6)


func play_stagecleared_stream():
	play_audio_stream(7)


func resume_music_stream():
	match music_playing:
		"ingame":
			play_music_stream(ingame_music_stream_position)
		"mainmenu":
			play_music_stream(mainmenu_music_stream_position)
		"pausemenu":
			play_music_stream(pausemenu_music_stream_position)


func stop_steps():
	stop_stream(4)


func stop_stream(number):
	audio_streams[number].stop()


func update_audio_properties():
	if game_manager.music_on:
		if !audio_streams[0].playing:
			resume_music_stream()
	elif audio_streams[0].playing:
		pause_music_stream()
