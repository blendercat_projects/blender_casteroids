extends Control

export var markers_number : int = 4

const RED_TARGET_ARROW_PATH : String = "res://scenes/red_target_arrow.tscn"
const RED_TARGET_CIRCLE_PATH : String = "res://scenes/red_target_circle.tscn"

var hud_enabled : bool = true

var red_targets_circles = []
var red_markers_arrows = []

var i_available_red_target_circle : int = -1
var i_available_red_marker_arrow : int = -1

var game_manager : Node


func _ready():
	game_manager = get_node("/root/GameManager")
	get_targets()
	enable_all_targets(false)
	enable_all_markers(false)


func create_red_target_arrow(index : int):
	var target = load(RED_TARGET_ARROW_PATH).instance()
	red_markers_arrows.append(target)
	target.name = "Red_marker_arrow" + str(index)
	$Red_Target_Arrows.add_child(target)


func create_red_target_circle(index : int):
	var target = load(RED_TARGET_CIRCLE_PATH).instance()
	red_targets_circles.append(target)
	target.name = "Red_target_circle" + str(index)
	$Red_Target_Circles.add_child(target)


func drone_in_camera(drone : KinematicBody, camera : Camera):
	var target : TextureRect = null
	if !drone.target_assigned:
		target = show_available_red_circle()
		if target:
			if drone.drone_enabled:
				target.set_drone_target(drone, camera)
			else:
				target.set_target_enabled(false)
	return target


func drone_out_camera(drone : KinematicBody, camera : Camera):
	var marker : TextureRect = null
	if !drone.marker_assigned:
		marker = show_available_red_arrow()
		if marker:
			if drone.drone_enabled:
				marker.set_drone_marker(drone, camera)
			else:
				marker.set_marker_enabled(false)
	return marker


func enable_all_markers(enable : bool):
	for i_marker in range(len(red_markers_arrows)):
		enable_mark_arrow(i_marker, enable)


func enable_all_targets(enable : bool):
	for i_target in range(len(red_targets_circles)):
		enable_target_circle(i_target, enable)


func enable_mark_arrow(index : int, enable : bool):
	red_markers_arrows[index].set_marker_enabled(enable)


func enable_target_circle(index : int, enable : bool):
	red_targets_circles[index].set_target_enabled(enable)


func get_available_red_arrow():
	var marker = null
	update_available_red_arrow()
	if i_available_red_marker_arrow > -1:
		marker = red_markers_arrows[i_available_red_marker_arrow]
	return marker


func get_available_red_circle():
	var target = null
	update_available_red_circle()
	if i_available_red_target_circle > -1:
		target = red_targets_circles[i_available_red_target_circle]
	return target


func get_targets():
	for i_target in range(markers_number):
		create_red_target_arrow(i_target)
		create_red_target_circle(i_target)


func show_available_red_arrow():
	var marker = get_available_red_arrow()
	if marker:
		marker.set_marker_enabled(true)
	return marker


func show_available_red_circle():
	var target = get_available_red_circle()
	if target:
		target.set_target_enabled(true)
	return target


func update_available_red_arrow():
	i_available_red_marker_arrow = 0
	while ((i_available_red_marker_arrow < len(red_markers_arrows)) and (red_markers_arrows[i_available_red_marker_arrow].marker_enabled)):
		i_available_red_marker_arrow += 1
	if i_available_red_marker_arrow == len(red_markers_arrows):
		i_available_red_marker_arrow = -1


func update_available_red_circle():
	i_available_red_target_circle = 0
	while ((i_available_red_target_circle < len(red_targets_circles)) and (red_targets_circles[i_available_red_target_circle].target_enabled)):
		i_available_red_target_circle += 1
	if i_available_red_target_circle == len(red_targets_circles):
		i_available_red_target_circle = -1


func _process(_delta):
	if hud_enabled:
		pass
		#search_drones()
