extends Area

#Velocidad del proyectil
var speed = 100

# Activat
var motion_enabled = false

# Gun parent
var gun_parent = null

# Main node
var bullets_node = null

# Timer
var timer : Timer = null

# Fire effect
var fire_effect : Particles = null

# Explosions effects
var explosion_effect : Particles = null
var asteroid_explosion_effect : Particles = null

# Missil mesh
var missil : Spatial = null

var ship_node : KinematicBody = null


func _ready():
	timer = get_node("Timer")
	missil = get_missil_spatial()
	fire_effect = get_fire_effect()
	explosion_effect = get_explosion_effect()
	asteroid_explosion_effect = get_asteroid_explosion_effect()


func disable_missil():
	enable_fire_effect(false)
	show_missil(false)
	motion_enabled = false


func enable_fire_effect(enable : bool):
	fire_effect.emitting = enable


func enable_missil():
	global_transform.basis = gun_parent.global_transform.basis
	global_transform.origin = gun_parent.global_transform.origin
	show_missil(true)
	motion_enabled = true
	if timer == null:
		timer = find_node("Timer")
	timer.start(timer.wait_time)
	enable_fire_effect(true)


func get_asteroid_explosion_effect():
	return get_node("Asteroid_explosion")


func get_explosion_effect():
	return get_node("Explosion_effect")


func get_fire_effect():
	return get_node("Missil/missil/foc_particles")


func get_missil_spatial():
	return get_node("Missil")


func set_gun_parent(ob):
	gun_parent = ob
	set_ship()
	#gun_parent.add_child(self)


func set_ship():
	ship_node = gun_parent.get_parent().get_parent()


func set_bullets_node(node):
	bullets_node = node
	bullets_node.add_child(self)


func shot_asteroid_explosion_effect():
	asteroid_explosion_effect.fire(global_translation)


func shot_explosion_effect():
	explosion_effect.fire(global_translation)


func show_missil(show : bool):
	missil.visible = show


func _physics_process(dt):
	if motion_enabled:
		#Mover el proyectil
		translate(Vector3.FORWARD * speed * dt)


func _on_Bullet_body_entered(body):
	#Si se ha chocado contra un drone, calcular el daño
	shot_explosion_effect()
	if "Gunner" in body.name:
		body.kill_drone()
	if "Watcher" in body.name:
		body.kill_drone()
	if "Obstacle" in body.name:
		ship_node.obstacles_area.obstacle_touched_by_ship(body)
		shot_asteroid_explosion_effect()
	# Desactivar missil al chocar
	disable_missil()


func _on_Projectil_jugador_area_entered(area):
	shot_explosion_effect()
	if "Obstacle" in area.name:
		area.visible = false
		shot_asteroid_explosion_effect()
	disable_missil()


func _on_Timer_timeout():
	#Si se ha activado el timer de 2 segundos, destruir el proyectil
	if motion_enabled:
		disable_missil()
