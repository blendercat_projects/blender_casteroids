extends Control

export var showing_time : float = 2.0

var timer : Timer
var game_manager : Node


func _ready():
	visible = false
	timer = $Timer
	game_manager = get_parent()
	game_manager.create_fade(self)
	visible = true
	game_manager.fade_out()
	autoset_timer()


func autoset_timer():
	timer.wait_time = showing_time


func force_quit():
	timer.stop()
	game_manager.fade_in()


func set_new_scene():
	game_manager.title_showed(self)


func start():
	start_timer()


func start_timer():
	timer.start(timer.wait_time)


func _on_Timer_timeout():
	game_manager.fade_in()
