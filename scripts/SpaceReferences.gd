extends Spatial

var particle_systems = []
var last_velocity : float


func _ready():
	get_systems()
	last_velocity = particle_systems[0].process_material.gravity.z


func enable_systems(enable : bool):
	if enable:
		restore_velocity()
	else:
		pause_velocity()
	
	for part_system in particle_systems:
		part_system.emitting = enable


func get_systems():
	particle_systems = get_children()


func pause_velocity():
	last_velocity = particle_systems[0].process_material.gravity.z


func restore_velocity():
	for part_system in particle_systems:
		part_system.process_material.gravity.z = last_velocity
