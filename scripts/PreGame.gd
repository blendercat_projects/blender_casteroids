extends Spatial

const SOUND_FOLDER_PATH = "sound/dialogs/"

const CAPTAIN_TEXT_MESSAGES = ["Control! I'm reaching the target position",
								"OK, Control. Executing aproaching protocol",
								"What the hell is that?",
								"Yes, it seems space garbagge",
								"Control, the object is rotating faster",
								"Ship navigation is disabled! I can't move anyway!"]
# https://notevibes.com/ Daniel-UK
const CAPTAIN_MESSAGES_PREFIX = "Captain_"

const CONTROL_TEXT_MESSAGES = ["OK, Captain. Proceed to aproaching protocol",
								"Take care, Captain. We know nothing about it",
								"It could be an old satelit",
								"Captain, we're receiving strange lectures",
								"Captain, it would be better you get out there",
								"Captain? Are you there? Captain?"]
# https://notevibes.com/ Kate-US
const CONTROL_MESSAGES_PREFIX = "Control_"

var ship : Spatial
var pregame_camera : Camera

var initial_camera_position : Position3D
var initial_ship_position : Position3D
var enter_ship_camera_position : Position3D
var ship_contact_position : Position3D
var zoom_done_camera_position : Position3D
var inside_ship_camera_position : Position3D
var tween : Tween

var captain_audio : AudioStreamPlayer
var control_audio : AudioStreamPlayer
var ambient_audio : AudioStreamPlayer

var messages_label : Label
var messages_panel : Panel

var dialogs_label_timer : Timer

var captain_next_msg : int = 0
var control_next_msg : int = 0

var captain_msg_panel_style : StyleBoxFlat
var control_msg_panel_style : StyleBoxFlat
var captain_msg_font_style : Font
var control_msg_font_style : Font


func _ready():
	ship = $Ship
	pregame_camera = $Camera
	
	initial_camera_position = $Initial_Camera_position
	initial_ship_position = $Initial_Ship_position
	enter_ship_camera_position = $Enter_Ship_Camera_position
	ship_contact_position = $Ship_contact_position
	zoom_done_camera_position = $Zoom_done_Camera_position
	inside_ship_camera_position = $Ship/Inside_ship_Camera_position
	tween = $Tween
	
	captain_audio = $Message_Control/Audio_Messages_Captain
	control_audio = $Message_Control/Audio_Messages_Control
	
	messages_panel = $Message_Control/Messages_Panel
	messages_label = messages_panel.get_node("Messages_Label")
	ambient_audio = $Ambient_Audio
	
	dialogs_label_timer = $Dialogs_Label_Timer
	
	load_styles()
	
	hide_message_panel()
	init_music()
	ship.global_transform.origin = initial_ship_position.translation
	pregame_camera.global_transform.origin = initial_camera_position.translation
	var scene_fader = load("res://scenes/SceneFader.tscn").instance()
	add_child(scene_fader)
	scene_fader.fade_out()


func hide_message_panel():
	messages_panel.visible = false


func init_music():
	ambient_audio.stream = load("res://sound/music/PreGame_Peace.ogg")
	ambient_audio.stream.loop = false
	ambient_audio.play()


func init_scene():
	show_captain_message(0)
	move_camera(initial_camera_position, enter_ship_camera_position, 10.0)
	move_ship(initial_ship_position, ship_contact_position, 12.0)


func load_styles():
	captain_msg_panel_style = load("res://ui/captain_msg_panel_style.tres")
	control_msg_panel_style = load("res://ui/control_msg_panel_style.tres")
	captain_msg_font_style = load("res://fonts/vila_madalena_pregame_captain.tres")
	control_msg_font_style = load("res://fonts/vila_madalena_pregame_control.tres")


func move_camera (initial_pos : Position3D, final_pos : Position3D, motion_time : float):
	pregame_camera.global_transform.origin = initial_pos.translation
	tween.interpolate_property(pregame_camera, "translation", initial_pos.transform.origin, final_pos.transform.origin, motion_time, Tween.TRANS_CIRC, Tween.EASE_OUT)
	tween.start()


func move_ship (initial_pos : Position3D, final_pos : Position3D, motion_time : float):
	ship.global_transform.origin = initial_pos.translation
	tween.interpolate_property(ship, "translation", initial_pos.transform.origin, final_pos.transform.origin, motion_time, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()


func play_captain_audio(audio_number : int):
	var number_string : String = "%0*d" % [2, audio_number]
	var audiofile_name : String = SOUND_FOLDER_PATH + CAPTAIN_MESSAGES_PREFIX + number_string + ".ogg"
	captain_audio.stream = load(audiofile_name)
	captain_audio.stream.loop = false
	captain_audio.play()


func play_control_audio(audio_number : int):
	var number_string : String = "%0*d" % [2, audio_number]
	var audiofile_name : String = SOUND_FOLDER_PATH + CONTROL_MESSAGES_PREFIX + number_string + ".ogg"
	control_audio.stream = load(audiofile_name)
	control_audio.stream.loop = false
	control_audio.play()


func set_captain_message(msg_number : int):
	messages_label.text = CAPTAIN_TEXT_MESSAGES[msg_number]


func set_control_message(msg_number : int):
	messages_label.text = CONTROL_TEXT_MESSAGES[msg_number]


func set_message_font_style(font_style : Font):
	messages_label.set('custom_fonts/font', font_style)


func set_message_panel_style(panel_style : StyleBoxFlat):
	messages_panel.set('custom_styles/panel', panel_style)


func set_new_scene():
	pass


func show_captain_message(msg_number : int, msg_time : float = 3.0):
	set_message_panel_style(captain_msg_panel_style)
	set_message_font_style(captain_msg_font_style)
	set_captain_message(msg_number)
	play_captain_audio(msg_number)
	messages_panel.visible = true
	dialogs_label_timer.start(msg_time)
	captain_next_msg += 1


func show_control_message(msg_number : int, msg_time : float = 3.0):
	set_message_panel_style(control_msg_panel_style)
	set_message_font_style(control_msg_font_style)
	set_control_message(msg_number)
	play_control_audio(msg_number)
	messages_panel.visible = true
	dialogs_label_timer.start(msg_time)
	control_next_msg += 1

func start():
	init_scene()


func _on_Tween_tween_completed(object, key):
	print("Tween completed: ", object.name, " ", key)


func _on_Audio_Messages_Captain_finished():
	show_control_message(control_next_msg)


func _on_Audio_Messages_Control_finished():
	show_captain_message(captain_next_msg)


func _on_Dialogs_Label_Timer_timeout():
	hide_message_panel()
