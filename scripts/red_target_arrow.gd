extends TextureRect

const X_MAX : int = 100
const Y_MAX : int = 100

var marker_enabled : bool = false
var drone_target : KinematicBody = null
var camera : Camera = null

var stage_manager : Node

var texture_size : Vector2


func _ready():
	stage_manager = get_node("/root/GameManager/StageManager")
	texture_size = rect_size * rect_scale


func calculate_target_position():
	var window_s : Vector2 = get_viewport().get_visible_rect().size
	var initial_pos = Vector2(int((window_s.x * 0.5) - (rect_pivot_offset.x/2)), int((window_s.y * 0.5) - ((rect_pivot_offset.y/2)*3)))
	var pos : Vector2 = initial_pos
	
	var ship_pos : Vector3 = stage_manager.ship.global_transform.origin
	var drone_pos : Vector3 = drone_target.global_transform.origin
	var distance : Vector3 = stage_manager.ship.to_local(drone_pos)
	var f_distance : float = ship_pos.distance_to(drone_pos)
	
	# Lateral position
	var rot_degrees : float = 0.0
	if distance.x < (-f_distance * (camera.fov / 360)):
		pos.x = -int(rect_pivot_offset.x/2)
		rot_degrees -= 45
	elif distance.x > (f_distance * (camera.fov / 360)):
		pos.x = window_s.x - int((rect_pivot_offset.x/2)*3)
		rot_degrees += 45
	# Vertical position
	if distance.y < (-f_distance * (camera.fov / 360)):
		pos.y = window_s.y - int((rect_pivot_offset.y/2)*3)
		if rot_degrees == 0.0:
			rot_degrees = 180.0
		else:
			rot_degrees += (rot_degrees*2)
	elif distance.y > (f_distance * (camera.fov / 360)):
		pos.y = -int(rect_pivot_offset.y/2)
	else:
		rot_degrees = (rot_degrees*2)
	
	# Check if drone is at exact back position
	if pos == initial_pos:
		# Drone just in back position
		pos.x = initial_pos.x
		pos.y = -int(rect_pivot_offset.y/2)
		rect_rotation = 0
	else:
		# Drone just in back position
		rect_rotation = rot_degrees
	
	return pos


func set_drone_marker(drone : KinematicBody, cam : Camera):
	drone_target = drone
	camera = cam


func set_marker_enabled(enabled : bool, drone : KinematicBody = null):
	drone_target = drone
	marker_enabled = enabled
	visible = enabled


func set_marker_position(pos : Vector2):
	rect_position = pos


func _process(_delta):
	if marker_enabled:
		if drone_target and camera:
			if drone_target.drone_enabled:
				set_marker_position(calculate_target_position())
