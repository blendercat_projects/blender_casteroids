extends Control


onready var play_button : Button = find_node("PlayButton")

var all_mainmenu_scene : Spatial
var main_menu_panel : Panel
var game_manager : Node
var continue_button : Button
var fade : Tween
var homeworld_tween : Tween


func _ready():
	visible = false
	all_mainmenu_scene = get_parent()
	game_manager = get_parent().get_parent()
	main_menu_panel = find_node("MainMenuPanel")
	continue_button = find_node("ContinueButton")
	homeworld_tween = all_mainmenu_scene.get_node("Tween_HomeWorld")
	autoset_continue_button()
	autoset_tween_homeworld()
	game_manager.create_fade(self)
	visible = true
	game_manager.fade_out()
	play_button.grab_focus()
	#config_main_menu()


func autoset_continue_button():
	continue_button.disabled = !game_manager.is_saved_game
	if continue_button.disabled:
		continue_button.mouse_default_cursor_shape = Control.CURSOR_ARROW
	else:
		continue_button.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND


func autoset_tween_homeworld():
	pass


func config_main_menu():
	rect_size = game_manager.current_resolution


func faded_in():
	#queue_free()
	pass


func faded_out():
	pass


func start():
	game_manager.delete_fader()


func _on_PlayButton_button_up():
	game_manager.load_stage(1)
	all_mainmenu_scene.queue_free()


func _on_OptionsButton_button_up():
	pass # Replace with function body.


func _on_QuitButton_button_up():
	game_manager.quit()
