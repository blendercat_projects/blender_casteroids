extends Particles

var sound : AudioStreamPlayer3D


func _ready():
	sound = $SoundStream


func fire(position : Vector3):
	global_transform.origin = position
	sound.play()
	emitting = true
