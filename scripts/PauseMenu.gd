class_name PauseMenu extends Control


onready var resume_button = find_node("ResumeButton")

var game_manager : Node
var stage_manager : Node


func _ready():
	stage_manager = get_parent()
	game_manager = stage_manager.get_parent()


func _on_ExitButton_button_up():
	game_manager.exit_to_main_menu()
	queue_free()


func _on_OptionsButton_button_up():
	pass # Replace with function body.


func _on_ResumeButton_button_up():
	stage_manager.unpause_game()


func _on_ResumeButton_button_down():
	print("Resume...")


func _on_PauseMenu_about_to_show():
	resume_button.grab_focus()

