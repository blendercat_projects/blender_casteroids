extends KinematicBody

const BULLETS_PARENT_PATH : String = "res://scenes/Bullets.tscn"
const OBSTACLES_AREA_PATH : String = "res://scenes/Obstacles_Area.tscn"

var missil_prefab : PackedScene = load("res://scenes/Projectil_jugador.tscn")

#Velocidad de giro y avance del jugador
var turn_speed : float
var speed : float

#Booleano de disparo
var disparo_preparado

var xAxis : float
var yAxis : float
var fire : bool
var pause_bt : bool

#Nodos de los disparadores
var gunpoints = []

#Variables de vida y vida máxima del jugador
var HP
var HP_MAX
var HP_BALIZA
var HP_BALIZA_MAX

# Timer para cadencia de disparo
var cadence_timer : Timer

# Pool de missils
var missils = []

# Control management
var control_management : bool = false

# Barras vida
var barra_hp_nave : Spatial = null
var barra_hp_baliza : Spatial = null

# Ship pause
var ship_paused : bool = false

# Ship collisions
var ship_collision : KinematicCollision = null

# Obstacles area
var obstacles_area : Area = null
var last_object_touched = null

# Game manager link
var stage_manager

var bullets_parent_node : Spatial

var hud : Control

var space_references : Spatial
var space_references_tree : SceneTree

# Sound
var rocket_out_sound_stream : AudioStreamPlayer3D


func _ready():
	stage_manager = get_parent()
	hud = find_node("HUD")
	space_references = find_node("SpaceReferences")
	space_references_tree = space_references.get_tree()
	
	#Guardar los gunpoints
	get_gunpoints()
	
	# Preparar pool de missils
	setup_missils()
	
	# Timer para cadencia de disparo
	cadence_timer = get_node("Cadence_Timer")
	
	#Preparamos el disparo
	disparo_preparado = true
		
	#Establecer la vida y la vida maxima
	HP_MAX = 100
	HP = HP_MAX
	HP_BALIZA_MAX = 10000
	HP_BALIZA = HP_BALIZA_MAX
	
	turn_speed = 1
	speed = 5 # 2000
	
	barra_hp_nave = get_barra_hp_nave()
	barra_hp_baliza = get_barra_hp_baliza()
	
	rocket_out_sound_stream = $Ship_Sounds/Rocket_Out_Effect
	
	enable_control(false)


func autoset_obstacles_area():
	obstacles_area = get_obstacles_area()


func disparar_bala(gun):
	gun.enable_missil()


func enable_control(enabled : bool):
	control_management = enabled


func enable_space_references(enable : bool):
	#space_references.enable_systems(enable)
	space_references_tree.set_pause(!enable)
	print("Paused : ", !enable)


func get_barra_hp_baliza():
	return get_node("Barra_HP_baliza")


func get_barra_hp_nave():
	return get_node("Barra_HP_nave")


func get_beacon():
	return get_node("../Beacon")


func get_gunpoints():
	var gunpoints_parent = get_node("Gun_points")
	gunpoints = gunpoints_parent.get_children()


func get_input():
	# Touches ready for screen touching devices
	var right = Input.is_action_pressed('ui_right')# || right_touch
	var left = Input.is_action_pressed('ui_left')# || left_touch
	var up = Input.is_action_pressed('ui_up')# || left_touch
	var down = Input.is_action_pressed('ui_down')# || left_touch
	
	pause_bt = Input.is_action_just_released("ui_cancel")
	
	fire = Input.is_action_just_released("ui_accept")
	
	xAxis = 0.0
	yAxis = 0.0
	if right:
		xAxis = -1.0
	if left:
		xAxis = 1.0
	if up:
		yAxis = -1.0
	if down:
		yAxis = 1.0


func get_obstacles_area():
	var obs_area = load(OBSTACLES_AREA_PATH).instance()
	self.add_child(obs_area)
	return obs_area


func init_random_obstacles():
	obstacles_area.init_random_obstacles()


func pause_ship():
	ship_paused = true
	cadence_timer.paused = true
	hud.visible = false
	#enable_space_references(false)
	enable_control(false)


func process_collision():
	if ship_collision.collider != last_object_touched:
		if "Obstacle" in ship_collision.collider.name:
			obstacles_area.obstacle_touched_by_ship(ship_collision.collider)
			stage_manager.show_fire_explosion(global_transform.origin)
			stage_manager.show_asteroid_explosion(global_transform.origin)
			rebre_dany(10.0)
		else:
			stage_manager.show_fire_explosion(global_transform.origin)
			rebre_dany(10.0)
		last_object_touched = ship_collision.collider


func process_input(delta):
	if pause_bt:
		stage_manager.pause_game()
	if !control_management:
		return
	
	# Motion process
	rotate_object_local(Vector3.UP, delta * turn_speed * xAxis)
	rotate_object_local(Vector3.RIGHT, delta * turn_speed * yAxis)
	var facing = global_transform.basis.z * -speed * delta
	ship_collision = move_and_collide(facing)
	if ship_collision:
		process_collision()

	# Fire action process
	if fire:
		if disparo_preparado:
			rocket_out_sound_stream.play()
			#Poner el booleano a FALSE
			disparo_preparado = false
			cadence_timer.start(cadence_timer.wait_time)
			
			#Activar los cuatro disparadores
			for ms in missils:
				disparar_bala(ms)


func rebre_dany(dmg):
	#Calcular el daño recibido de un ataque de drone
	self.HP -= dmg
	
	#Reducir el indicador de salud de la nave
	barra_hp_nave.scale.z = float(HP)/float(HP_MAX)
	
	#Si se ha agotado la salud, pasar a la escena de Game Over
	if self.HP <= 0:
		stage_manager.end_stage(false)


func rebre_dany_baliza(dmg):
	HP_BALIZA -= dmg
	update_barra_hp_baliza(float(HP_BALIZA)/float(HP_BALIZA_MAX))
	if HP_BALIZA <= 0:
		stage_manager.end_stage()


func setup_missil():
	var bala = missil_prefab.instance()
	return bala


func setup_missils():
	bullets_parent_node = load(BULLETS_PARENT_PATH).instance()
	stage_manager.call_deferred("add_child", bullets_parent_node)
	for gp in gunpoints:
		var missil = setup_missil()
		missils.append(missil)
		missil.set_gun_parent(gp)
		missil.set_bullets_node(bullets_parent_node)


func unpause_ship():
	print("Unpause ship")
	ship_paused = false
	cadence_timer.paused = false
	hud.visible = true
	#enable_space_references(true)
	enable_control(true)


func update_barra_hp_baliza(total : float): # PENDING TO MOVE BEACON FUNCTION HERE
	#Reducir el tamaño de la barra de vida roja en la pantalla del jugador
	barra_hp_baliza.scale.z = total


func _process(delta):
	if !ship_paused:
		get_input()
		process_input(delta)


func _on_Timer_timeout():
	#Si el contador ha agotado su tiempo, podemos disparar
	disparo_preparado = true
