extends TextureRect

const X_MAX : int = 100
const Y_MAX : int = 100

var target_enabled : bool = false
var drone_target : KinematicBody = null
var camera : Camera = null

var stage_manager : Node

var texture_size : Vector2


func _ready():
	stage_manager = get_node("/root/GameManager/StageManager")
	texture_size = rect_size * rect_scale


func calculate_target_position():
	var pos : Vector2 = camera.unproject_position(drone_target.global_transform.origin)
	
	pos = pos - texture_size

	return pos


func set_drone_target(drone : KinematicBody, cam : Camera):
	drone_target = drone
	camera = cam


func set_target_enabled(enabled : bool, drone : KinematicBody = null):
	drone_target = drone
	target_enabled = enabled
	visible = enabled


func set_target_position(pos : Vector2):
	rect_global_position = pos


func _process(_delta):
	if target_enabled:
		if drone_target and camera:
			if drone_target.drone_enabled:
				set_target_position(calculate_target_position())
