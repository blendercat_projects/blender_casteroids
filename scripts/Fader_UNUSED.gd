extends Tween

enum FADE_TYPE {FADEIN, FADEOUT}

export(FADE_TYPE) var fade_type = FADE_TYPE.FADEIN

export var target_node_name : String = ""

var _target_node = null
export var _property_name : String = ""
export var _from_property : String = "" # TODO: NOT POSIBLE
export var _to_property : String = "" # TODO: NOT POSSIBLE
export var _time : float = 1.0
export var _transition_type = Tween.TRANS_LINEAR
export var _transition_inout = Tween.EASE_IN_OUT


func _ready():
	_target_node = get_target_node()


func activate():
	if _target_node:
		self.interpolate_property(_target_node, _property_name,
		_from_property, _to_property, _time,
		_transition_type, _transition_inout)


func get_target_node():
	return get_node(target_node_name)


func set_tween(target_node = _target_node, property_name = _property_name, from_property = _from_property, to_property = _to_property, time = _time, transition_type = _transition_type, transition_inout = _transition_inout):
	_target_node = target_node
	_property_name = property_name
	_from_property = from_property
	_to_property = to_property
	_time = time
	_transition_type = transition_type
	_transition_inout = transition_inout

