extends KinematicBody

#Deadzones del Joystick
var deadZone
var deadZone_vel

#Velocidad de giro y avance del jugador
var velocidad_giro
var velocidad_avance

#Booleano de disparo
var disparo_preparado

#Valor del eje de velocidad del Joystick
var velAxis

#Nodos de la cabeza del jugador
var camL = null
var camR = null

#Nodos de los disparadores
var gunpoint1 = null
var gunpoint2 = null
var gunpoint3 = null
var gunpoint4 = null

#Variables de vida y vida máxima del jugador
var HP
var HP_MAX


func _ready():
	#Activar el Joystick
	Input.connect("joy_connection_changed",self,"joy_con_changed")
	
	#Guardar las dos cabezas
	camL = get_node("/root/GameManager/StageManager/Ship/Head/Hbox/VCL/ViewportLeft/HeadL/")
	camR = get_node("/root/GameManager/StageManager/Ship/Head/Hbox/VCR/ViewportRight/HeadR/")
	
	#Guardar los gunpoints
	gunpoint1 = get_node("/root/GameManager/StageManager/Ship/Gunpoint1")
	gunpoint2 = get_node("/root/GameManager/StageManager/Ship/Gunpoint2")
	gunpoint3 = get_node("/root/GameManager/StageManager/Ship/Gunpoint3")
	gunpoint4 = get_node("/root/GameManager/StageManager/Ship/Gunpoint4")
	
	#Preparamos el disparo
	disparo_preparado = true
	
	#Inicializar deadzones del mando
	deadZone = 0.5
	deadZone_vel = 5
	
	#Establecer la vida y la vida maxima
	HP_MAX = 100
	HP = HP_MAX
	
	velocidad_giro = 1
	velocidad_avance = 20


func _process(delta):
	
	# A partir de aquí leemos el input del mando y movemos la nave
	#
	# NOTA IMPORTANTE:
	# Según el mando, puede que tengas que modificar las constantes de los ejes y
	# los botones (ej. JOY_BUTTON_0, JOY_AXIS_1...). Asimismo, también puede ser
	# que tengas que aplicar unas correcciones u otras a las lecturas
	
	
	if Input.get_connected_joypads().size() > 0:
		
		#Abrir fuego si se pulsa el disparador (siempre que el booleano esté a true)
		if Input.is_joy_button_pressed(0,JOY_BUTTON_0):
			if disparo_preparado:
				#Poner el booleano a FALSE
				disparo_preparado = false
				
				#Activar los cuatro disparadores
				disparar_bala(gunpoint1)
				disparar_bala(gunpoint2)
				disparar_bala(gunpoint3)
				disparar_bala(gunpoint4)
		
		#Leemos los ejes del Joystick
		var xAxis = Input.get_joy_axis(0,JOY_AXIS_0)*(-1)
		var yAxis = Input.get_joy_axis(0,JOY_AXIS_1)*(1)
		
		#Leemos el eje de velocidad
		
		# El eje de velocidad da valores entre -1 y 1
		# Lo transformamos para que dé valores entre 0 y 100
		# (También puede que tengamos que invertir el eje, según el mando)
		velAxis = (Input.get_joy_axis(0, JOY_AXIS_2)*(-100) + 100) / 2.0
		
		# Establecemos una deadZone cercana a cero
		if velAxis < deadZone_vel:
			velAxis = 0
		
		#Rotar la NAVE y las CABEZAS en función de las lecturas de los ejes del JOYSTICK
		if abs(xAxis) > deadZone: #Eje X del mando -> eje horizontal del mundo (Y)
			self.rotation.y += delta * velocidad_giro * xAxis
			camL.rotation.y += delta * velocidad_giro * xAxis
			camR.rotation.y += delta * velocidad_giro * xAxis
		if abs(yAxis) > deadZone: #Eje Y del mando -> eje vertical del mundo (X)
			self.rotation.x += delta * velocidad_giro * yAxis
			camL.rotation.x += delta * velocidad_giro * yAxis
			camR.rotation.x += delta * velocidad_giro * yAxis
		
		#Mover la nave en función del velAxis
		var facing = -global_transform.basis.z*velAxis*velocidad_avance*delta
		move_and_slide(facing)
		
		#Rotar las cabezas
		camL.set_translation(get_translation())
		camR.set_translation(get_translation())
		
func disparar_bala(gun):
	
	#Instanciar una bala al nodo 'gun' (que se corresponderá a un gunpoint)
	var bala = preload("res://scenes/Projectil_jugador.tscn").instance()
	gun.add_child(bala)


func _on_Timer_timeout():
	
	#Si el contador ha agotado su tiempo, podemos disparar
	disparo_preparado = true
	
func atacar(dmg):
	#Calcular el daño recibido de un ataque de drone
	self.HP -= dmg
	
	#Reducir el indicador de salud de la nave
	get_node("/root/GameManager/StageManager/Ship/Barra_HP_nave").scale.z = float(HP)/float(HP_MAX)
	
	#Si se ha agotado la salud, pasar a la escena de Game Over
	if self.HP <= 0:
		get_tree().change_scene("res://scenes/GameOver.tscn")
